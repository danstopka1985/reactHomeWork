import React, { Component } from 'react'


export default class Bn extends Component {

    f = () => {
        let text = this.props.c,
            cb = this.props.clickHandler;

        if (cb) {
            cb.call(null, text);
        }
    };

    render(){
        return (
                <button onClick={this.f}>{this.props.c}</button>
        )
    }

}