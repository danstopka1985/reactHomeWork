import React, { Component } from 'react';
import ReactCSSOM from 'react-cssom';

export default class MediumHeader extends Component {

    render() {
        return <h3 align="left">{this.props.text}</h3>;
    }

}