import React, { Component } from 'react'


export default class SmartButton extends Component {

    f = () => {
        alert(this.props.message);
    };

    render() {
        return <button onClick={this.f}>{this.props.caption}</button>;
    }
}