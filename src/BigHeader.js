import React, { Component } from 'react'


export default class BigHeader extends Component {

    render() {
        return <h1 align="center">{this.props.text}</h1>
    }

}