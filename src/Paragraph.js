import React, { Component } from 'react'


export default class Paragraph extends Component {

    render() {
        let spanStyle = {color: 'red', fontSize: '2em'};
        let firstLatter = this.props.text[0].toUpperCase();
        let textWithoutFirstLetter = this.props.text.substring(1);
        return <p><span style={spanStyle}>{firstLatter}</span>{textWithoutFirstLetter}</p>;
    }

}