import React, { Component } from 'react'
import Bn from './Bn'


export default class Calc extends Component {

    keyPress = (event) => {
         if(+event.key >= 0 && +event.key <= 9) {
             if (this.state.value === 0){
                 this.setState({value: event.key});
             } else {
                 this.setState({value: this.state.value += event.key});
             }
         }
    };

    ff = (a) => {
        function evil(fn) {
            return new Function('return ' + fn)();
        }

        if (this.state.value !== '') {

            if (a >= 0 && a <= 9) {
                if (this.state.value === 0) {
                    this.setState({value: a});
                } else {
                    this.setState({value: this.state.value += a});
                }
            }
            let lastS = (this.state.value + '').substring(this.state.value.length - 1);
            if (lastS >= '0' && lastS <= '9' && (a === '+' || a === '-' || a === '/' || a === '*')){
                this.setState({value: this.state.value += a});
            }
            if (lastS >= '0' && lastS <= '9' && (a === '=')){
                this.setState({value: evil(this.state.value)});
            }

        }
    };

    power = () => {
        this.setState({value: 0});
    };

    constructor(props) {
        super(props);
        this.state = {value: ''};
    }

    render(){
        let style = {color: 'blue', textAlign: 'right'};
        return (
            <div onKeyPress={this.keyPress}>
                <button onClick={this.power}>Power/Reset</button>
                <br/>
                <input type="text" style={style} align="right" readOnly value={this.state.value}/>
                <br/>
                <Bn c="1" clickHandler={this.ff}/><Bn c="2" clickHandler={this.ff}/><Bn c="3" clickHandler={this.ff}/><br/>
                <Bn c="4" clickHandler={this.ff}/><Bn c="5" clickHandler={this.ff}/><Bn c="6" clickHandler={this.ff}/><br/>
                <Bn c="7" clickHandler={this.ff}/><Bn c="8" clickHandler={this.ff}/><Bn c="9" clickHandler={this.ff}/><br/>
                <Bn c="0" clickHandler={this.ff}/><Bn c="+" clickHandler={this.ff}/><Bn c="-" clickHandler={this.ff}/><br/>
                <Bn c="*" clickHandler={this.ff}/><Bn c="/" clickHandler={this.ff}/><Bn c="=" clickHandler={this.ff}/>

            </div>
        )
    }

}